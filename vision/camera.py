import cv2
import numpy as np

class Camera(object):
    def __init__(self, port=0):
        self.capture = cv2.VideoCapture(port)

    def get_frame(self):
        (status, frame) = self.capture.read()
        return frame
    
    def get_normalized_frame(self, frame = None):
        # setting frame to None just in case we want to test this with a simple image etc
        """
        Normalize an image based on its Saturation channel. Returns BGR version
        of the image.
        """
        if frame is None:
            frame = self.get_frame()
        
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        frame[:, :, 1] = cv2.equalizeHist(frame[:, :, 1])
        frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)
        return frame