import cv2
import numpy as np

MAX_HEIGHT = 300
MAX_WIDTH = 530

DEFAULT_DELAY_MS = 1
# this is represented in BGR format as the default OpenCV format
COLORS = {
    'red': (0, 0, 255),
    'green': (0, 255, 0),
    'blue': (255,0,0),
    'black': (0, 0, 0),
    'white': (255, 255, 255),
    'yellow': (0, 255, 255)
}

class GUI(object):
    # window names
    MW_NAME = "Robot Vision"
    DEMO_NAME = "Object recognition window"
    CALIBRATIONS = 'CALIBRATIONS'

    # trackbar names
    TB_HUE_HIGH = 'Hue High'
    TB_HUE_LOW = 'Hue Low'
    TB_SAT_HIGH = 'Sat High'
    TB_SAT_LOW = 'Sat Low'
    TB_VAL_HIGH = 'Val High'
    TB_VAL_LOW = 'Val Low'
    
    # member attributes

    frame = None
    modified_frame = None
    static_testing = False
    calibrations = None
    pitch_polygon = []


    def __init__(self, calibrations, static_testing = False):
        def update_calibrations(inp):
            c_robot = calibrations['robots']
            c_robot['hue_high'] = cv2.getTrackbarPos(self.TB_HUE_HIGH, self.CALIBRATIONS)
            c_robot['hue_low'] = cv2.getTrackbarPos(self.TB_HUE_LOW, self.CALIBRATIONS)
            c_robot['sat_high'] = cv2.getTrackbarPos(self.TB_SAT_HIGH, self.CALIBRATIONS)
            c_robot['sat_low'] = cv2.getTrackbarPos(self.TB_SAT_LOW, self.CALIBRATIONS)
            c_robot['val_high'] = cv2.getTrackbarPos(self.TB_VAL_HIGH, self.CALIBRATIONS)
            c_robot['val_low'] = cv2.getTrackbarPos(self.TB_VAL_LOW, self.CALIBRATIONS)

        self.calibrations = calibrations
        cv2.namedWindow(self.MW_NAME)
        cv2.moveWindow(self.MW_NAME, 320, 300)
        cv2.namedWindow(self.DEMO_NAME)
        cv2.moveWindow(self.DEMO_NAME, 320, 0)
        cv2.namedWindow(self.CALIBRATIONS, cv2.WINDOW_NORMAL)
        cv2.moveWindow(self.CALIBRATIONS, 852, 0)

        c_robot = calibrations['robots']

        cv2.createTrackbar(self.TB_HUE_LOW, self.CALIBRATIONS, c_robot['hue_low'], 90, update_calibrations)
        cv2.createTrackbar(self.TB_HUE_HIGH, self.CALIBRATIONS, c_robot['hue_high'], 90, update_calibrations)
        cv2.createTrackbar(self.TB_SAT_LOW, self.CALIBRATIONS, c_robot['sat_low'], 255, update_calibrations)
        cv2.createTrackbar(self.TB_SAT_HIGH, self.CALIBRATIONS, c_robot['sat_high'], 255, update_calibrations)
        cv2.createTrackbar(self.TB_VAL_LOW, self.CALIBRATIONS, c_robot['val_low'], 255, update_calibrations)
        cv2.createTrackbar(self.TB_VAL_HIGH, self.CALIBRATIONS, c_robot['val_high'], 255, update_calibrations)
        
        self.static_testing = static_testing 
        if self.static_testing:
            self.frame = cv2.imread("samples/test1.png")

    def draw_static(self):
        self.modified_frame = self.frame
        self.draw_pitch()
        
        cv2.imshow(self.MW_NAME, self.frame)
        cv2.setMouseCallback(self.MW_NAME, self.pitch_listener)
        # draw the processed image
        cv2.imshow(self.DEMO_NAME, self.modified_frame)

        # waiting for a key to get pressed within some time, without this call frame does not update properly
        cv2.waitKey(0)

    def update(self, delta_t, frame, modified_frame, data):
        self.frame = frame
        self.modified_frame = modified_frame

        #self.frame = self.camera.get_normalized_frame()
        # draw the camera feed to the main window
        self.draw_angles(data)
        self.draw_ball(frame, data)
        self.draw_ball_text(data)
        self.draw_robots_text(data)
        self.draw_pitch()
        self.draw_separating_lines()
        self.draw_fps(delta_t)
        self.draw_crops()

        cv2.imshow(self.MW_NAME, self.frame)
        # cv2.setMouseCallback(self.MW_NAME, self.pitch_listener)
        # draw the processed image
        cv2.imshow(self.DEMO_NAME, self.modified_frame)
        # update the window so as not to be resized
    

    def draw_fps(self, delta_t):
        self.draw_text("FPS: " + str(1/delta_t), 20, 20)

    def draw_crops(self):
        self.draw_text("Y Top: " + str(self.calibrations['croppings']['y1']), 20, 60)
        self.draw_text("Y Bottom: " + str(self.calibrations['croppings']['y2']), 20, 80) 

    def draw_ball_text(self, data):
        center = str(data['ball']['center'] or '')
        self.draw_text("Ball: " + center, 20, 40)

    def draw_robots_text(self, data):
        for robot in data['robots']:
            if robot != None and robot['center']:
                center = robot['center']

                self.draw_text(str(center), center[0] - 30, MAX_HEIGHT - center[1] - 30)
    
    def draw_text(self, text, x, y, frame=None, color=None, thickness=1, size=0.3,):
        if frame is None:
            frame = self.frame
        if color is None:
            color = COLORS['white']
        cv2.putText(frame, text, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, size, color, thickness)
    
    def draw_pitch(self):        
        if len(self.pitch_polygon) > 2:
            # transform pitch polygon representation to the matrix representation opencv requires
            pts = np.array(self.pitch_polygon, np.int32)
            pts = pts.reshape((-1,1,2))
            cv2.polylines(self.frame,[pts],True,(0,255,255))

    def draw_ball(self, frame, data):
        if not data:
          pass

        center = data['ball']['center']
        if center is None:
            return
        else:
            center = (data['ball']['center'][0], MAX_HEIGHT - data['ball']['center'][1])

        if center[0] and center[1]:
            frame_height, frame_width, _ = frame.shape
            self.draw_line(
                frame, ((int(center[0]), 0), (int(center[0]), frame_height)), 1)
            self.draw_line(
                frame, ((0, int(center[1])), (frame_width, int(center[1]))), 1)
    def draw_line(self, frame, points, thickness=2):
        if points is not None:
            cv2.line(frame, points[0], points[1], COLORS['red'], thickness)

    def draw_angles(self, data):
        for robot in data['robots']:
            if robot != None:
                x_angle, y_angle = robot['angle']
                x_pos, y_pos = robot['center']
                y_pos = MAX_HEIGHT - y_pos
                if x_angle != 0 or y_angle !=0:
                    mult = 20
                    cv2.line(self.frame, (int(mult*x_angle + x_pos), int(mult*y_angle + y_pos)), (int(x_pos), int(y_pos)), COLORS['yellow'], thickness=2)
                

    def draw_separating_lines(self):
        for value in self.calibrations['field_croppings'].values():
            cv2.line(self.frame, (value, 0), (value, 480), COLORS['blue'], thickness=1)

    def pitch_listener(self, event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.pitch_polygon.append([x,y])
