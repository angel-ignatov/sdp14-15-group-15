import cv2
import json
from gui import GUI
import time
from camera import Camera
import numpy as np
import operator

CALIBRATIONS_FILE = 'calibrations.json'

MAX_HEIGHT = 300
MAX_WIDTH = 530

class Vision(object):
    calibrations = None
    camera = None
    frame = None

    def __init__(self, file_path=None):
        self.camera = Camera()

        if file_path is None:
            self.calibrations = get_calibrations()
        else:
            self.calibrations = get_calibrations(file_path)
        crops = self.calibrations['croppings']
        assert crops['y2'] - crops['y1'] == MAX_HEIGHT
        assert crops['x2'] - crops['x1'] == MAX_WIDTH

    def recognize_ball(self):
        modified_frame = self.frame
        c_ball = self.calibrations['ball_frame']
        open_kernel = np.ones((c_ball['morph_open'], c_ball['morph_open']), np.uint8)

        # the inrange function
        modified_frame = cv2.blur(modified_frame, (c_ball['blur'], c_ball['blur']))
        modified_frame = cv2.cvtColor(modified_frame, cv2.COLOR_BGR2HSV)

        red_mask = cv2.inRange(modified_frame,(c_ball['hue1_low'], c_ball['sat_low'], c_ball['val_low']), (c_ball['hue1_high'], c_ball['sat_high'], c_ball['val_high']))
        violet_mask = cv2.inRange(modified_frame,(c_ball['hue2_low'], c_ball['sat_low'], c_ball['val_low']), (c_ball['hue2_high'], c_ball['sat_high'], c_ball['val_high']))
        modified_frame = cv2.bitwise_or(red_mask, violet_mask);

        # reducing noise significantly
        modified_frame = cv2.morphologyEx(modified_frame, cv2.MORPH_OPEN, open_kernel)
        
        contours, h = cv2.findContours(modified_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        contour = self.find_contour_in_range(contours, c_ball['area_low'], c_ball['area_high'])

        # no ball detected
        if contour is None:
            return 0, None, modified_frame

        center, radius = self.get_contour_center(contour)
        return radius, center, modified_frame

    def find_contour_in_range(self, contours, bottom, top):
        areas = [cv2.contourArea(c) for c in contours]
        for contour in contours:
            area = cv2.contourArea(c)
            if area >= bottom and area <= top:
                return contour
        return None

    def extract_box_coordinates(self, frame):
        non_zero = frame.nonzero()
        if len(non_zero[0]) < 1:
            return 0,0,0,0
        min_x = min(non_zero[1])
        min_y = min(non_zero[0])
        max_x = max(non_zero[1])
        max_y = max(non_zero[0])
        return min_x, max_x, min_y, max_y
    
    def recognize_plates(self):
        c_plate = self.calibrations['robots']
        erode_kernel = np.ones((c_plate['erode'],c_plate['erode']), np.uint8)
        dilate_kernel = np.ones((c_plate['dilate'],c_plate['dilate']), np.uint8)
        open_kernel = np.ones((c_plate['morph_open'],c_plate['morph_open']), np.uint8)
        close_kernel = np.ones((c_plate['morph_close'], c_plate['morph_close']), np.uint8)

        modified_frame = self.frame

        # first dilate the colors for less dark shadows
        modified_frame = cv2.dilate(modified_frame, dilate_kernel)
        #modified_frame = cv2.erode(modified_frame, np.ones((c_plate['erode'],c_plate['erode']), np.uint8))

        modified_frame = cv2.blur(modified_frame, (c_plate['blur'],c_plate['blur']))

        cv2.imshow('Robot Vision', modified_frame)

        # the inrange function to detect the colors
        modified_frame = cv2.cvtColor(modified_frame, cv2.COLOR_BGR2HSV)
        modified_frame = cv2.inRange(modified_frame, (c_plate['hue_low'], c_plate['sat_low'], c_plate['val_low']), (c_plate['hue_high'], c_plate['sat_high'], c_plate['val_high']))        
        modified_frame = cv2.morphologyEx(modified_frame, cv2.MORPH_OPEN, open_kernel)

        # not really necessary here
        modified_frame = cv2.morphologyEx(modified_frame, cv2.MORPH_CLOSE, close_kernel)
        modified_frame = cv2.erode(modified_frame, erode_kernel)

        # divide the pitch into four zones
        area_frames = self.divide_world(modified_frame)
        
        # robot return values
        ret_values = [None,None,None,None]
        i = 0
        for plate_frame in area_frames:
            # get the min_max plate for each of the plate frame
            min_x, max_x, min_y, max_y = self.get_contour_plates(plate_frame)
            self.get_contour_plates(plate_frame)

            # get the reduced frame for later angle detection
            reduced_frame = plate_frame[min_y : max_y, min_x : max_x]
            
            # calculate the center of the plate
            min_gx, min_gy = self.convert_area_to_pitch((min_x, min_y), i)
            max_gx, max_gy = self.convert_area_to_pitch((max_x, max_y), i)
            center = (min_gx + (max_gx - min_gx)/2, min_gy + (max_gy - min_gy)/2)

            # angle detection
            angle = self.get_plate_angle(reduced_frame, min_gx, max_gx, min_gy, max_gy)
            # append the return values
            ret_values[i] = {'center': center, 'angle': angle}
            i += 1

        return modified_frame, ret_values

    def get_contour_plates(self, frame):
        contours, h = cv2.findContours(frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        areas = [cv2.contourArea(c) for c in contours]
        filtered_areas = []
        i = 0
        for area in areas:
            if area > 140 and area < 1500:
                filtered_areas.append(contours[i])
            i += 1
        if not filtered_areas:
            return 0,0,0,0
        true_contour = np.vstack(filtered_areas)
        
        rect =  cv2.boundingRect(true_contour)
        return rect[0], rect[0] + rect[2], rect[1], rect[1] + rect[3]
        
        

    def get_plate_angle(self, mask_frame, min_x, max_x, min_y, max_y):
        if min_x != 0:
            self.debug_val = max_x
            width = max_x - min_x
            height = max_y - min_y
            if width < 15 or height < 15:
                return 0,0
            contours, hierarchy = cv2.findContours(mask_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            filtered_contours = [contour for contour in contours if cv2.contourArea(contour) > 25]

            if not filtered_contours:
                return 0, 0

            stacked_conts = np.vstack(filtered_contours)
            area = cv2.minAreaRect(stacked_conts)
            box = cv2.cv.BoxPoints(area)
            cv2.drawContours(self.frame, [np.int0(box)], 0, (0,255,0), offset=(min_x, min_y))
            
            #corner_points = self.find_corners(mask_frame)
            frame = self.frame[min_y:max_y, min_x:max_x].copy()
            
            # here we try and recognize the black dot on the robot
            # Fill the dummy frame
            white_frame = frame.copy()
            x, y = self.find_dot(frame, white_frame, width, height)
            cv2.circle(self.frame, (int(x+min_x), int(y+min_y)), 2, (0,0,255), -1)

            return self.find_angle_from_points([x, y], box)
        # could not recognize anything
        return 0, 0

    def find_dot(self, frame, white_frame, width, height):
        
        dot_c = self.calibrations['robot_dot']
        radius = dot_c['radius']
        cv2.rectangle(white_frame, (0, 0), (width, height), (255, 255, 255), -1)
        cv2.circle(white_frame, (width / 2, height / 2), radius, (0, 0, 0), -1)
        frame = cv2.bitwise_or(frame, white_frame)


        # getting dark colors is best in BGR
        frame = cv2.dilate(frame, np.ones((dot_c['dilate'],dot_c['dilate']), np.uint8))

        #if self.debug_val < 300:
        #    cv2.imshow("Object recognition window", frame)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        frame = cv2.inRange(frame, (dot_c['hue_low'],dot_c['sat_low'],dot_c['val_low']),(dot_c['hue_high'],dot_c['sat_high'],dot_c['val_high']))

        frame = cv2.morphologyEx(frame, cv2.MORPH_OPEN, np.ones((dot_c['morph_open'],dot_c['morph_open']), np.uint8))

        #if self.debug_val > 300:
        #    cv2.imshow("Robot Vision", frame)

        contours, hierarchy = cv2.findContours(frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        if len(contours) < 1:
            return 0, 0

        center, radius = self.get_contour_center(self.get_largest_contour(contours))

        x, y = center
        return x, y
        

    def find_corners(self, frame):
        white_points = cv2.findNonZero(frame)
        top_point = min(white_points, key=lambda x : x[0][1])
        bottom_point = max(white_points, key=lambda x : x[0][1])
        left_point = min(white_points, key=lambda x : x[0][0])
        right_point = max(white_points, key=lambda x : x[0][0])
        return top_point, right_point, bottom_point, left_point
    
    def find_angle_from_points(self, dot_point, points):
        x, y = dot_point
        # square distance calculation function
        dist_sq = lambda x, y : (x[0]-y[0])**2 + (x[1]-y[1])**2
        # calculate distance for each of the points (between dot and box points)
        dist_sqs = np.array([dist_sq(dot_point, point) for point in points])

        # sort distances
        sorted_distances = dist_sqs.argsort()
        # get sorted points by distances
        points_by_distance = [points[x] for x in sorted_distances[::-1]]

        # furthest two points
        p1_x, p1_y = points_by_distance[3]
        p2_x, p2_y = points_by_distance[2]

        # closest two points
        p3_x, p3_y = points_by_distance[1]
        p4_x, p4_y = points_by_distance[0]

        # middle points
        m1_x, m1_y = (p1_x + p2_x)/2.0, (p1_y + p2_y)/2.0
        m2_x, m2_y = (p3_x + p4_x)/2.0, (p3_y + p4_y)/2.0

        # direction vector
        d_x, d_y = m2_x - m1_x, m2_y - m1_y
        length = (d_x**2 + d_y**2)**0.5
        if length < 2 or length > 100:
            return 0,0 
        return d_x/length, d_y/length

    def convert_area_to_pitch(self, coordinates, area):
        lines = self.calibrations['field_croppings']
        x,y = coordinates
        if area == 1:
            x += lines['line1']
        if area == 2:
            x += lines['line2']
        if area == 3:
            x += lines['line3']
        return x, y

    def get_world_state(self):
        data = {'ball': {'center': (0, 0), 'radius': 0}, 'robots':[None, None, None, None]}
        # main call for getting all the data from vision
        #self.frame = self.camera.get_normalized_frame()
        self.frame = self.camera.get_frame()
        self.crop_frame()
        ball_mask = None
        ball_radius, ball_center, ball_mask = self.recognize_ball()
        data['ball']['radius'] = ball_radius
        # wr need to flip the y coordinate for the planner
        if ball_center is not None:
            data['ball']['center'] = (ball_center[0], MAX_HEIGHT - ball_center[1])
        else:
            data['ball']['center'] = None
        
        modified_frame, plate_data = self.recognize_plates()

        # flip the y coordinate for the planner
        for robot_data in plate_data:
            if robot_data['center'] is not None:
                robot_data['center'] = (robot_data['center'][0], MAX_HEIGHT - robot_data['center'][1])
            

        data['robots'] = plate_data

        # see both ball recognition and robot recognition
        modified_frame = cv2.bitwise_or(ball_mask, modified_frame)
        return data, modified_frame

    def crop_frame(self):
        crops = self.calibrations['croppings']
        self.frame = self.frame[crops['y1']:crops['y2'], crops['x1']:crops['x2']]

    def get_largest_contour(self, contours):
        """
        Find the largest of all contours.
        """
        areas = [cv2.contourArea(c) for c in contours]
        return contours[np.argmax(areas)]

    def get_contour_center(self, contour):
        """
        Find the center of a contour by minimum enclousing circle approximation.

        Returns: ((x, y), radius)
        """
        return cv2.minEnclosingCircle(contour)

    def divide_world(self, frame):
        fcs = self.calibrations['field_croppings']
        # creating a world with four fields separated by lines in the calibrations
        return [
            frame[:, :fcs['line1']],
            frame[:, fcs['line1']:fcs['line2']],
            frame[:, fcs['line2']:fcs['line3']],
            frame[:, fcs['line3']:]
        ]

def get_calibrations(filepath=None):
    # gets calibrations from file    
    if filepath is None:
        filepath = CALIBRATIONS_FILE

    ret_val = None
    file_handle = open(filepath, 'r')
    ret_val = json.load(file_handle)
    file_handle.close()
    return ret_val

def dump_calibrations(calibrations, filepath=None):
    # writes calibrations to file
    if filepath is None:
        filepath = CALIBRATIONS_FILE

    file_handle = open(filepath, 'w')
    json.dump(calibrations, file_handle)
    file_handle.close()

def get_default_calibrations():
    return {
        'croppings' : {
            'x1': 65, # keep width 530 pixels
            'x2': 595,
            'y1': 40, # keep height 300 pixels
            'y2': 340
        },
        'field_croppings' : {
            "line3": 410,
            "line2": 257,
            "line1": 105
        },
        'ball_frame': {
                'hue1_low': 0,
                'hue1_high': 10,
                'hue2_low': 170,
                'hue2_high': 180,
                'sat_low': 170,
                'sat_high': 255,
                'val_low': 100,
                'val_high': 255,
                'blur': 5,
                'morph_open': 5
            },
        'robots': {
                'hue_low': 55,
                'hue_high': 80,
                'sat_low': 100,
                'sat_high': 200,
                'val_low': 95,
                'val_high': 200,
                'blur': 5,
                'erode': 2,
                'dilate': 2,
                'morph_open': 6,
                'morph_close': 10
        }
    }

if __name__ == '__main__':
    vision = Vision()
    gui = GUI(vision.calibrations)
    try:
        if gui.static_testing:
            gui.draw_static()
        else:
            t1 = time.time()
            handle = True
            while(handle):
                # program clock
                t2 = time.time()
                # change of time between frames in seconds
                delta_time = t2-t1
                t1 = t2
                # getting all the data from the world state
                data, modified_frame = vision.get_world_state()

                # update the gui
                gui.update(delta_time, vision.frame, modified_frame, data)

                # uncomment to see the format vision data is returned
                #print data, '\n'

                key = cv2.waitKey(5) & 0xFF
                if key == ord('q'):
                    handle = False

    except KeyboardInterrupt:
        print "Bye bye"
