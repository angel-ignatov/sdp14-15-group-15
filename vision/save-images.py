import cv2
import numpy as np
from camera import Camera
import time
from gui import COLORS

DEFAULT_DELAY_MS = 1

class GUI(object):
    MW_NAME = "Robot Vision"
    
    # member attributes
    index = 0
    camera = None
    frame = None
    modified_frame = None

    def __init__(self):
        cv2.namedWindow(self.MW_NAME)
        self.camera = Camera()
        
        
    def draw(self, delta_t):
        self.frame = self.camera.get_frame()
        self.modified_frame = self.frame
        self.draw_fps(delta_t)
        
        cv2.imshow(self.MW_NAME, self.frame)
        cv2.setMouseCallback(self.MW_NAME, self.pitch_listener)
        
        # press a to save file
        a = cv2.waitKey(33) & 0xFF
        if a == ord('a'):
            print "Pressed a"

            cv2.imwrite("test"+str(self.index)+".jpg",gui.frame)
            print "Saving file as test%d.jpg!" % self.index 
            self.index += 1


        # waiting for a key to get pressed within some time, without this call frame does not update properly
        cv2.waitKey(DEFAULT_DELAY_MS)
    
    def draw_fps(self, delta_t):
        self.draw_text("FPS: " + str(1/delta_t), 20, 20)
    
    def draw_text(self, text, x, y, frame=None, color=None, thickness=1, size=0.3,):
        if frame is None:
            frame = self.frame
        if color is None:
            color = COLORS['white']
        cv2.putText(frame, text, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, size, color, thickness)
    
    def pitch_listener(self, event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            print "Coordinate x: " + str(x) 
            print "Coordinate y: " + str(y)
            print self.frame[x][y] 



if __name__ == '__main__':
    gui = GUI()
    try:
        t1 = time.time()
        while(True):
            # program clock
            t2 = time.time()
            # change of time between frames in seconds
            delta_time = t2-t1
            t1 = t2

            gui.draw(delta_time)



    except KeyboardInterrupt:
        print "Bye bye"
