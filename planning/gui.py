import cv2

class GUI:
    NAME = "Planner"

    def update(self, x):
        self.planner.paused    = cv2.getTrackbarPos("Pause", self.NAME)
        self.planner.min_power = cv2.getTrackbarPos("Min Power", self.NAME)

        self.planner.motor_adjust = cv2.getTrackbarPos("Right Motor Adjust", self.NAME) / 100.0

        self.planner.turn_constant = cv2.getTrackbarPos("Turn Constant", self.NAME) / 100.0
        self.planner.turn_factor = cv2.getTrackbarPos("Turn Factor", self.NAME) / 100.0
        self.planner.turn_power_factor = cv2.getTrackbarPos("Turn Power Factor", self.NAME)

        self.planner.distance_factor = cv2.getTrackbarPos("Distance Factor", self.NAME)

        pass

    def __init__(self, planner):
        self.planner = planner

        cv2.namedWindow(self.NAME)
        cv2.moveWindow(self.NAME, 0, 0)

        cv2.createTrackbar("Pause", self.NAME, planner.paused, 1, self.update)

        cv2.createTrackbar("Min Power", self.NAME, planner.min_power, 90, self.update)

        cv2.createTrackbar("Right Motor Adjust", self.NAME, planner.motor_adjust * 100, 100, self.update)

        cv2.createTrackbar("Turn Constant", self.NAME, planner.turn_constant * 100, 500, self.update)
        cv2.createTrackbar("Turn Factor", self.NAME, planner.turn_factor * 100, 500, self.update)
        cv2.createTrackbar("Turn Power Factor", self.NAME, planner.turn_power_factor, 50, self.update)

        cv2.createTrackbar("Distance Factor", self.NAME, planner.distance_factor, 50, self.update)
