import math
from gui import GUI
from models.models import zone_edges, furthest_zone_point, get_key_goal_targets
from collections import namedtuple
Point = namedtuple('Point', ['x', 'y'])

class SimplePlanner:

    """Different Tasks that the planner can do
        intercept is catching a moving ball
        retreive is getting a stationary ball
    """

    #Tasks
    INTERCEPT = 1
    RETREIVE = 2
    PASS = 3
    ALIGN_BALL = 4
    SHOOT = 5

    MOVE_THRESHOLD = 0.7
    GRAB_THRESHOLD = 0.4
    DISTANCE_TO_BALL_THRESHOLD = 40
    
    turn_threshold = 0.20
    
    EVADE_WIDTH = 80

    #Preivous Commands
    STRAIGHT = 0
    TURN = 1

    # Configurable things...
    paused = False
    min_power = 60

    motor_adjust = 0.65

    turn_constant = 2.0
    turn_factor   = 0.4
    turn_power_factor = 40
    distance_factor = 20

    # Other things...
    last_turn_angle = None

    # Before shooting, check a few times we are actually in line.
    align_checks = 0

    # During "Critical" tasks, the task list should not change.
    critical_task = False

    def __init__(self, world, robot_controller, attack_defend):
        self.world = world
        self.controller = robot_controller
        self.task_num = 0
        self.task_list = []
        self.evade_point = None
        
        if attack_defend == 'attack':
          self.robot = world.our_attacker
        else:
          self.robot = world.our_defender
        
        self.gui = GUI(self)

    def plan(self):
        """
        Main entry point to class,
        sends the positions to the current task
        """

        if self.paused == 1:
            print "===================PAUSED==================="
            return 0

        if self.robot.has_ball(self.world.ball):
            print "I've got the ball"

        ball = self.world.ball

        if self.task_num < len(self.task_list):
            task, data = self.task_list[self.task_num]
            if task == self.INTERCEPT:
                self.critical_task = False
                self.intercept()
            elif task == self.RETREIVE:
                self.critical_task = False
                self.retreive()
            elif task == self.PASS:
                self.critical_task = False
                self.pass_ball(data)
            elif task == self.ALIGN_BALL:
                self.critical_task = False
                self.align(self.world.ball)
            elif task == self.SHOOT:
                self.critical_task = True
                self.kick_goal()
            return 0
        else:
            self.critical_task = False
            return 1

    def set_task_list(self, task_list):
        if self.critical_task:
            raise Exception("Don't change the task list during a critical task")

        self.task_list = task_list
        self.task_num = 0

    # Intercept a pass between their defender and their attacker
    # Crude intercept:
    # Split the difference between their y coordinates, and go there.
    def old_intercept(self):
        their_defender = self.world.their_defender
        their_attacker = self.world.their_attacker

        diff = abs(their_attacker.y - their_defender.y)

        intercept_y = (diff / 2) + min(their_attacker.y, their_defender.y)

        intercept_point = Point(self.robot.zone_center[0], intercept_y)

        if self.align(intercept_point):
            if self.move_to_point(intercept_point):
                self.task_num +=1

    # Returns true if their defender is facing us
    def their_defender_facing_us(self):
        their_defender = self.world.their_defender

        if self.world.our_side == 'left':
            return their_defender.angle >= (math.pi / 2) and their_defender.angle <= (3/2 * math.pi)
        else:
            return their_defender.angle <= (math.pi / 2) or their_defender.angle >= (3/2 * math.pi)

    # Slightly more intelligent intercept, that takes their defender's angle into account
    def intercept(self):
        their_defender = self.world.their_defender

        if (not self.their_defender_facing_us()):
            self.old_intercept()
            return

        x_diff = self.robot.zone_center[0] - their_defender.x

        if their_defender.angle <= 0:
            y_diff = 0
        else:
            y_diff = float(x_diff) / math.tan(their_defender.angle)

        intercept_y = their_defender.y + y_diff

        # Make sure we aren't off the bottom
        edges = zone_edges(self.robot.zone_polygon)
        intercept_y = max(edges[1][1] + 30, intercept_y)
        # Or the top
        intercept_y = min(edges[0][1] - 30, intercept_y)

        intercept_point = Point(self.robot.zone_center[0], intercept_y)

        if self.align(intercept_point):
            if self.move_to_point(intercept_point):
                self.task_num +=1

    def move_to_point(self, pitch_object, threshold = DISTANCE_TO_BALL_THRESHOLD):
        if self.robot.distance(pitch_object) < threshold:
            print "I'm at the point!"
            return True
        else:
            self.controller.move(
                self.min_power,
                self.min_power * self.motor_adjust,
                self.get_forward_time(self.robot.distance(pitch_object))
            )
            return False

    def get_turn_time(self, angle):
        time = self.turn_constant + (self.turn_factor * math.sin(abs(angle)/2))
        print "turn time", time
        return time

    def get_forward_time(self, distance):
        time = distance / self.distance_factor
        print "forward time", time
        return time

    def align(self, pitch_object, threshold = turn_threshold):
        turn_angle = self.robot.get_turn_angle(pitch_object)

        print "turn angle", turn_angle

        self.adjust_turn_parameters(turn_angle, pitch_object, threshold)

        if abs(turn_angle) < threshold:
            print "I'm in line"
            self.last_turn_angle = None
            return True
        else:
            print "I'm not in line. Turning."
            self.turn(turn_angle)
            return False

    def adjust_turn_parameters(self, turn_angle, pitch_object, threshold):
        if self.last_turn_angle and (abs(turn_angle) > threshold):
          if (abs(turn_angle) < abs(self.last_turn_angle)):
            print "***********Undershoot***********"
            self.turn_factor = min(3.0, self.turn_factor + 0.1)
          else:
            print "***********Overshoot***********"
            self.turn_factor = max(0, self.turn_factor - 0.1)

        self.last_turn_angle = turn_angle

    def retreive(self):
        if self.align(self.world.ball):
            if self.move_to_point(self.world.ball):
                self.controller.grab()
                self.task_num +=1

    not_gots = 0
    kick_attempts = 0
    def kick_goal(self):
        goal = self.world.their_goal
        goal_points = get_key_goal_targets(goal, 60)

        if self.robot.has_ball(self.world.ball):
            self.not_gots = 0
        else:
            self.not_gots += 1

            if self.not_gots > 20:
                print "I've lost the ball"
                self.kick_attempts = 0
                self.task_num -= 1

        for goal_point in goal_points:
            if self.robot.can_we_shoot(self.world.their_defender, goal_point, self.EVADE_WIDTH):
                self.evade_point = None
                self.kick_attempts += 1

                print 'Aiming at ', goal_point

                if self.align(goal_point, 0.15):
                    self.align_checks += 1
                    # Check a few times we are indeed aligned.
                    if self.align_checks >= 5:
                        print '============'
                        print '***Shoot!***'
                        print '============'
                        self.controller.kick(300, 100)
                        self.task_num += 1
                        self.kick_attempts = 0
                else:
                    self.kick_attempts += 1
                    self.align_checks = 0

                break
            else:
                if self.kick_attempts > 50 and self.align(goal_point):
                    print self.kick_attempts, 'attempts at avoiding their defender. Kicking anyway'
                    self.controller.kick(200, 100)
                    self.kick_attempts = 0
                    self.task_num += 1

        else:
            self.kick_attempts += 1
            zone = self.world.pitch.zones[self.robot.zone]

            if not self.evade_point:
                cp = furthest_zone_point(zone, (self.robot.x, self.robot.y))
                self.evade_point = Point(self.robot.x, 2 * (cp[1] + self.robot.y) / 3)

            print 'Evading to ', self.evade_point

            if self.align(self.evade_point):
                if self.move_to_point(self.evade_point):
                    self.evade_point = None

    def pass_ball(self, target):
        if self.robot.can_we_shoot(self.world.their_attacker, target, self.EVADE_WIDTH):
            self.evade_point = None
        
            if self.align(target):
                self.controller.kick(200, 100)
                self.task_num += 1
        else:
            zone = self.world.pitch.zones[self.robot.zone]
            
            if not self.evade_point:
              cp = furthest_zone_point(zone, (self.robot.x, self.robot.y))
              self.evade_point = Point(self.robot.x, 2 * (cp[1] + self.robot.y) / 3)

            print 'Evading to ', self.evade_point

            if self.align(self.evade_point):
                if self.move_to_point(self.evade_point):
                    self.evade_point = None

    def turn(self, angle):
        pwr = self.get_power(abs(angle))
        #checks for direction of turn
        if angle > 0:
            self.controller.move(-pwr, pwr * self.motor_adjust, self.get_turn_time(angle))
        else:
            self.controller.move(pwr, -pwr * self.motor_adjust, self.get_turn_time(angle))

    def get_power(self, angle):
        """ Determinds the power to use for turning """
        power = self.min_power # + (self.turn_power_factor * math.sin(abs(angle)/2))
        return power

