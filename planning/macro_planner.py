"""
The planner for High(er) level strats
"""
from models.world import World
from planning.simple_planner import SimplePlanner


class MacroPlanner:

    def __init__(self, world_state, robot_controller):
        self.simple_planner = SimplePlanner(world_state, robot_controller, 'attack')
        self.state = None
        self.world_state = world_state
        self.robot = world_state.our_attacker

    """Decides if the ball has moved zones and if it plans
    resets the strategy, otherwise continues with current plan"""
    def plan(self):
        if not self.simple_planner.critical_task:
            if self.world_state.ball_position == World.OUR_DEFENDER:
                print 'Ball in our defender'
                if self.state != 0:
                    self.our_defender()
                    self.state = 0
            elif self.world_state.ball_position == World.THEIR_DEFENDER:
                print 'Ball in their defender'
                if self.state != 1:
                    self.their_defender()
                    self.state = 1
            elif self.world_state.ball_position == World.THEIR_ATTACKER:
                print 'Ball in their attacker'
                if self.state != 2:
                    self.their_attacker()
                    self.state = 2
            elif self.world_state.ball_position == World.OUR_ATTACKER:
                print 'Ball in our attacker'
                if self.state != 3:
                    self.our_attacker()
                    self.state = 3
            else:
                pass

        print 'our angle', self.robot.angle

        if self.simple_planner.plan():
            # We've reached the end of the task list.
            # Restart again
            self.state = -1

    def our_defender(self):
        tasks = [(SimplePlanner.ALIGN_BALL,None)]
        self.simple_planner.set_task_list(tasks)

    def our_attacker(self):
        self.simple_planner.controller.open_grabber()

        tasks = [(SimplePlanner.RETREIVE,None),(SimplePlanner.SHOOT, None)]

        self.simple_planner.set_task_list(tasks)


    def their_attacker(self):
        tasks = [(SimplePlanner.ALIGN_BALL,None)]
        self.simple_planner.set_task_list(tasks)

    def their_defender(self):
        tasks = [(SimplePlanner.INTERCEPT,None)]
        self.simple_planner.set_task_list(tasks)

