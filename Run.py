#!/usr/bin/env python

from vision.vision import Vision, dump_calibrations
from vision.gui import GUI
from postprocessing.postprocessing import Postprocessing
from preprocessing.preprocessing import Preprocessing
from models.world import World
from control.Controller import RobotController
import vision_stable.tools as tools
from cv2 import waitKey
from planning.macro_planner import MacroPlanner
import cv2
import serial
import warnings
import time
from models.models import get_key_goal_targets


warnings.filterwarnings("ignore", category=DeprecationWarning)


class Runner:
    """
    Primary source of robot control. Ties vision and planning together.
    """

    def __init__(self, pitch, color, our_side, attack_defend, video_port=0, comm_port='/dev/ttyACM0', comms=1):
        """
        Entry point for the SDP system.

        Params:
            [int] pitch                     0 - main pitch, 1 - secondary pitch
            [string] color                  the colour of the team we are (yellow or blue)
            [string] our_side               the side we're on - 'left' or 'right'
            [string] attack_defend          whether we are attacking or defending
            [int] video_port                port number for the camera
            [string] comm_port              port number for the arduino
        """
        assert pitch in [0, 1]
        assert color in ['yellow', 'blue']
        assert our_side in ['left', 'right']

        self.pitch = pitch

        # Set up the Arduino communications
        self.robot= RobotController(comm_port)

        if pitch == 0:
            self.calib_file = "vision/calibrations2.json"
        else:
            self.calib_file = "vision/calibrations.json"

        self.vision = Vision(self.calib_file)
        self.gui = GUI(self.vision.calibrations)

        # Set up postprocessing for vision
        self.postprocessing = Postprocessing()

        # Set up world
        self.world = World(our_side, self.calib_file)

        goal = self.world.their_goal
        goal_points = get_key_goal_targets(goal, 60)

        for goal_point in goal_points:
            print 'Goal at: ', goal_point

        # Set up main planner
        self.planner = MacroPlanner(self.world, self.robot)

        # Set up GUI
        self.color = color
        self.side = our_side
        self.preprocessing = Preprocessing()

    def run(self):
        """
        Ready your sword, here be dragons.
        """

        counter = 1L
        timer = time.clock()
        end = False
        
        skip = 0

        try:
            c = True
            self.robot.ping()
            while not end and c != 27:  # the ESC key
                t2 = time.time()
                # change of time between frames in seconds
                delta_time = t2-timer
                timer = t2

                # getting all the data from the world state
                data, modified_frame = self.vision.get_world_state()

                # update the gui
                self.gui.update(delta_time, self.vision.frame, modified_frame, data)

                # uncomment to see the format vision data is returned
                # print data, '\n'

                # model_positions, regular_positions = self.get_positions(frame, pre_options)
                self.world.update_positions(data)

                if self.robot.ready:
                    skip -= 1

                if self.robot.ready and skip <= 0:
                    # The robot is ready to accept a new command
                    end = self.planner.plan()
                    
                    skip = 1

                key = cv2.waitKey(4) & 0xFF
                if key == ord('q'):
                    end = True
                    self.save_calibrations()

                counter += 1

        finally:
            self.robot.stop()

    def save_calibrations(self):
        dump_calibrations(self.vision.calibrations, self.calib_file)

    def get_positions(self, frame, pre_options):
        """
        Work out the positions of all models.
        """

        # Apply preprocessing methods toggled in the UI
        preprocessed = self.preprocessing.run(frame, pre_options)

        frame = preprocessed['frame']
        if 'background_sub' in preprocessed:
            cv2.imshow('bg sub', preprocessed['background_sub'])

        model_positions, regular_positions = self.vision.locate(frame)
        model_positions = self.postprocessing.analyze(model_positions)

        return model_positions, regular_positions

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("pitch", help="[0] Main pitch, [1] Secondary pitch")
    parser.add_argument("side", help="The side of our defender ['left', 'right'] allowed.")
    parser.add_argument("color", help="The color of our team - ['yellow', 'blue'] allowed.")
    #parser.add_argument("attack_defend", help="Are we attacking or defending? - ['attack', 'defend']")
    parser.add_argument(
        "-n", "--nocomms", help="Disables sending commands to the robot.", action="store_true")

    args = parser.parse_args()
    if args.nocomms:
        c = Runner(
            pitch=int(args.pitch), color=args.color, our_side=args.side, attack_defend='attack', comms=0).run()
    else:
        c = Runner(
            pitch=int(args.pitch), color=args.color, our_side=args.side, attack_defend='attack').run()
