# -*- coding: utf-8 -*-
# import the serial library
import serial
from time import sleep
from threading import Thread

# handles the connection between the computer and the Arduino circuit

class CommsToArduino(object):
    # these should be hard-coded, the values should not change
    def __init__(self, port,
                 rate=115200,
                 timeout=0,
                 connected=False):
        self.isConnected = connected
        self.port = port
        self.comn = None # updated when we establish the connection
        self.rate = rate
        self.timeout = timeout
        self.connect()

    seqNo = False
    ready = True

    # this function establishes the connection between the devices
    # and updates the boolean variable isConnected
    def connect(self):
        if self.isConnected is False and self.comn is None:
            try:
                self.comn = serial.Serial(port=self.port,
                                          baudrate=self.rate,
                                          timeout=self.timeout)
                self.isConnected = True
            except OSError as ex:
                print("Cannot connect to Arduino.")
                print(ex.strerror)
                
    def create_checksum(self, args):
        """
        Creates the checksum that is used to verify the command.
        """
        
        sum = 0
        for arg in args:
            sum += abs(arg)
        return sum % 10

    def _write(self, command, seqNo, args):
        """
        Repeatedly sends the command to the robot until an OK is received.
        Then waits until DONE, and sets "ready" true.
        """

        received = None

        string_args = ' '.join(['%d' % x for x in args])
        checksum = self.create_checksum(args)

        while received != "DONE\r\n":
          command_string = "%s %d %s %d\r" % (command, seqNo, string_args, checksum)
          self.comn.write(command_string)
          sleep(0.2)
          received = self.comn.readline()
          
          if received == "Checksum failed\r\n":
            print "Checksum Failed", command_string
            seqNo = not seqNo

          if received == "Wat?\r\n":
            print "WAT WAT?", command_string

        print "  DONE"
        print
        self.ready = True

    def write(self, command, args):
        """
        Public interface for sending commands to the robot
        """

        if self.isConnected:
            self.ready = False
            self.seqNo = not self.seqNo

            thread = Thread(target = self._write, args = (command, self.seqNo, args))
            thread.start()
        else:
            print("Not connected to Arduino.")

class RobotController(CommsToArduino):
    """
    Robot_Controller for robot control.
    """
    #need to decide on useful variables, at this point it is not very clear what we will need. We set the available commands here, in order to excapsulate them
    #and prevent sending random strings to Arduino. This also makes changing their implementation at a later date easier.
    #
    def __init__(self, port):
        super(RobotController, self).__init__(port)
        self.current_speed = 0

    def handshake(self):
        received = ""
        self.write("P\r", "")
        while received != "P\r\n":
            received = self.comn.readline()
        print("Handshake Complete")

        # Note, we need to include \r in addition to \n as as different machines
    # may use either one for end-of-line character

    # Since the serial library supports parsing multiple arguments in a single
    # string command( using the next() function) this allows us to specify
    # certain arguments such as distance and degree. We can also give a series
    # of commands in one string

    def ping(self):
        self.write("P", "")

    def move(self, power_right, power_left, time):
        self.write("M", [power_right, power_left, time])

    def stop(self):
        self.write("S", "")

    def kick(self, time, power):
        self.write("K", [time, power])

    def grab(self):
        self.write("G", "")

    def open_grabber(self):
	self.write("O", "")

