#!/usr/bin/python
# -*- coding: utf-8 -*-
from Controller import RobotController
from Tkinter import Tk, Frame, BOTH

robot = RobotController("/dev/ttyACM0")
robot.handshake()

def forward(event):
    print("Moving forward")
    robot.move(100,100)

def right(event):
    print("Moving forward")
    robot.move(100,-100)

def left(event):
    print("Moving forward")
    robot.move(-100,100)

def backward(event):
    print("Moving backward")
    robot.move(-100,-100)

def kick(event):
    print("Kicking")
    robot.kick(400,100)

def stop(event):
    print("stopping")
    robot.stop()

def main():
    root = Tk()
    root.geometry("250x150+300+300")
    frame = Frame(root, width=100, height=100)
    root.bind("<KeyRelease>", stop)
    root.bind("w", forward)
    root.bind("s", backward)
    root.bind("a", right)
    root.bind("d", left)
    root.bind("e", kick)
    root.mainloop()  

if __name__ == '__main__':
    main()  
