###Installation

#### Linux/DICE

To install the Polygon library, download the [source](https://bitbucket.org/jraedler/polygon2/downloads/Polygon2-2.0.6.zip), navigate inside and execute `python setup.py install --user`.

To isntall Argparse for python, download [ArgParse](http://argparse.googlecode.com/files/argparse-1.2.1.tar.gz), extract and run `python setup.py install --user`. All done.

There are a couple more libraries required:
*serial* - can be removed if bluetooth is to be used

### Vision

* At the moment OpenCV + Python are being used. A [book](http://programmingcomputervision.com/downloads/ProgrammingComputerVision_CCdraft.pdf) on Computer Vision with OpenCV in Python is a decent starting point about what OpenCV can do.
* A detailed tutorial with examples and use cases can be found [here](https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_tutorials.html) - going through it can be handy to understand the code
* For OpenCV installation instructions please get in touch with others or have a look at the scripts in *vision/*

------
### Installing OpenCV

#### Linux/DICE
* Download [OpenCV](http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.8/opencv-2.4.8.zip/download)
* Extract the contents
* When on computers with with video feed, navigate to */disk/scratch/sdp/* and create a directory for OpenCV
* Copy extracted contents over to this directory
* Create directory `build` and navigate inside
* Execute 
```
cmake -D WITH_OPENCL=OFF -D WITH_CUDA=OFF -D BUILD_opencv_gpu=OFF -D BUILD_opencv_gpuarithm=OFF -D BUILD_opencv_gpubgsegm=OFF -D BUILD_opencv_gpucodec=OFF -D BUILD_opencv_gpufeatures2d=OFF -D BUILD_opencv_gpufilters=OFF -D BUILD_opencv_gpuimgproc=OFF -D BUILD_opencv_gpulegacy=OFF -D BUILD_opencv_gpuoptflow=OFF -D BUILD_opencv_gpustereo=OFF -D BUILD_opencv_gpuwarping=OFF -D CMAKE_INSTALL_PREFIX=~/.local ..
```
to install without GPU libraries
* Execute `make` and wait (for quite some time)
* Execute `make install`
* Run `ipython` and do `import cv2`, if all executes fine then you're set.