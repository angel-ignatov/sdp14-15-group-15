import vision7
import math

def predictedPosition(prev_x, prev_y, cur_x, cur_y, prev_time, cur_time, initial_velocity, angle):

	time = 0

	delta_x = math.abs(prev_x-cur_x) #calculate X displacement
	delta_y = math.abs(prev_y-cur_y) #calculate Y displacement

	delta_t = math.abs(prev_time - cur_time) #calculate period of time dt

	delta_vx = float(delta_x)/float(delta_t) #calculate X-component of the velocity
	delta_vy = float(delta_y)/float(delta_t) #calculate Y-component of the velocity

	init_vx = initial_velocity*math.cos(angle)
	init_vy = initial_velocity*math.sin(angle)


	accel_x = float(delta_vx)/float(delta_t) #calculate X-component of the scceleration
	accel_y = float(delta_vy)/float(delta_t)
	
	predicted_position_x = 0.5*accel_x*math.pow(time, 2) + init_vx*time + cur_x
	predicted_position_y = 0.5*accel_y*math.pow(time, 2) + init_vy*time + cur_y

	return predicted_position_x, predicted_position_y


