from pitch_object import PitchObject
from math import *
from Polygon.cPolygon import Polygon


ROBOT_WIDTH = 30
ROBOT_LENGTH = 45
ROBOT_HEIGHT = 10

class Robot(PitchObject):

    def __init__(self, pitch, zone, x, y, angle, velocity, width=ROBOT_WIDTH, length=ROBOT_LENGTH, height=ROBOT_HEIGHT):
        super(Robot, self).__init__(x, y, angle, velocity, width, length, height)
        self._pitch = pitch
        self._zone = zone
        self._grabber = 'open'

    @property
    def zone(self):
        return self._zone

    @property
    def zone_polygon(self):
        return self._pitch.zones[self.zone]

    # Get the mid point for our zone.
    @property
    def zone_center(self):
        return self.zone_polygon.center()

    #The front offset represents the distance in front of the robot that the grabber can still
    #successfully catch the ball. Currently the calculation for the area assumes that the
    #grabber is centered in the middle of the width of the robot.
    @property
    def grabber_area(self):
        # The distance between the center of the robot and the back of the grabber area
        front_offset = 20

        depth = 40
        width = 40

        front_left  = (self.x + front_offset + depth, self.y + (width / 2))
        front_right = (self.x + front_offset + depth, self.y - (width / 2))

        back_left  = (self.x + front_offset, self.y + (width / 2))
        back_right = (self.x + front_offset, self.y - (width / 2))

        area = Polygon((front_left, front_right, back_left, back_right))
        area.rotate(self.angle, self.x, self.y)
        return area

    def has_ball(self, ball):
        '''
        Get if the ball is in the grabber zone but may not have possession
        '''
        return self.grabber_area.isInside(ball.x, ball.y)

    def intercept_coordinates(self, ball):
        delta_x = abs(self.x - ball.x)
        delta_y = abs(self.y - ball.y)
        triangle_y = delta_x * tan(ball.angle)

        if(ball.y > self.y):
            y_coordinate = self.y + delta_y + triangle_y
        else:
            y_coordinate = self.y - delta_y - triangle_y

        x_coordinate = self.x;
        return (x_coordinate, y_coordinate)

    #The next three functions should be part of the vector class instead?
    def get_turn_angle(self, pitch_object):
        rel_ang = self.relative_angle(pitch_object)
        return self.angle_to_turn(rel_ang)

    def angle_to_turn(self, desired_angle):
            """
            Returns the angle(radians) that the robot needs to turn to be
            pointed at the ball.
            Positive is clockwise, Negative is anti-clockwise
            """

            print 'desired angle', desired_angle

            turn_angle = desired_angle - self.angle

            if turn_angle < -1*pi:
              turn_angle += 2*pi

            if turn_angle > pi:
              turn_angle -= 2*pi

            return turn_angle

    def relative_angle(self, pitch_object):
        """
        Returns the angle (radians) anticlockwise from the horizontal axis to the ball
        taking the center of the self as the origin.
        e.g. if the self  is at (10,10) and the ball is at (20,5)
        the result will be approx 5.819
        """
        rx = self.x
        ry = self.y

        #displacements
        dx = rx - pitch_object.x
        dy = ry - pitch_object.y

        return atan2(dy, dx) + pi

    #useful to find out if there is a robot from the other team in the path for the ball
    def get_pass_path(self, target):
        '''
        Gets a path represented by a Polygon for the area for passing ball between two robots
        '''
        robot_poly = self.get_boundary_polygon()
        target_poly = target.get_boundary_polygon()
        return Polygon(robot_poly[0], robot_poly[1], target_poly[0], target_poly[1])

# Width measures the front and back of an object
# Length measures along the sides of an object

# important to check alignment BEFORE calling this method!!

    def can_we_shoot(self, blocker_robot, ally_robot, width):
        # Polygon needs points in a clockwise or anti-clockwise manner to produce a convex polygon
        p1 = (self.x, self.y - width /  2)
        p2 = (self.x, self.y + width /  2)
        p3 = (ally_robot.x, ally_robot.y + width / 2)
        p4 = (ally_robot.x, ally_robot.y - width / 2)
        poly = Polygon([p1, p2, p3, p4])
        is_blocker_inside = poly.isInside(blocker_robot.x, blocker_robot.y)
        return not is_blocker_inside

    def __repr__(self):
        return ('zone: %s\nx: %s\ny: %s\nangle: %s\nvelocity: %s\ndimensions: %s\n' %
                (self._zone, self.x, self.y,
                 self.angle, self.velocity, (self.width, self.length, self.height)))

