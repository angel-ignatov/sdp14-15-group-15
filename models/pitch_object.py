from Polygon.cPolygon import Polygon
from vector import Vector
import math
import numpy as np

'''def savitzky_golay(y, window_size=3, order=3, deriv=0, rate=1):

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * math.factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')'''

class PitchObject(object):
    '''
    A class that describes an abstract pitch object
    Width measures the front and back of an object
    Length measures along the sides of an object
    '''

    def __init__(self, x, y, angle, velocity, width, length, height):
        if width < 0 or length < 0 or height < 0:
            raise ValueError('Object dimensions must be positive')
        else:
            self._width = width
            self._length = length
            self._height = height
            self._vector = Vector(x, y, angle, velocity)

        self.previous_angle = None

    @property
    def width(self):
        return self._width

    @property
    def length(self):
        return self._length

    @property
    def height(self):
        return self._height

    @property
    def angle(self):
        return self._vector.angle

    @property
    def velocity(self):
        return self._vector.velocity

    @property
    def x(self):
        return self._vector.x

    @property
    def y(self):
        return self._vector.y

    @property
    def vector(self):
        return self._vector

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, new_position):

        # Get the angle in radians

        try:
            # Vision returns a normalized vector for the angle.
            rad_angle = math.atan2(new_position['angle'][1], new_position['angle'][0])

            while rad_angle < 0:
              rad_angle += math.pi*2

            # smoothing
            if self.previous_angle is None:
                self.previous_angle = rad_angle
            else:
                temp = rad_angle
                rad_angle = (self.previous_angle + rad_angle)/2
                self.previous_angle = temp
            
            

        except KeyError:
            rad_angle = 0
            
        old_vector = self._vector

        if new_position['center'] is None:
            #this need editing
            return None

        new_x = new_position['center'][0]
        new_y = new_position['center'][1]

        if old_vector:
            d_x = old_vector.x - new_x
            d_y = old_vector.y - new_y
            speed = math.hypot(d_x, d_y)
        else:
            speed = 0

        self._vector = Vector(new_x, new_y, rad_angle, speed)

    def distance(self, pitch_object):
        """ Returns the distance using pythagoras' theorm"""

        dx = abs(self.x-pitch_object.x)
        dy = abs(self.y-pitch_object.y)

        distance =  math.sqrt(math.pow(dx,2) + math.pow(dy,2))
        return distance

    def get_bounding_polygon(self):
        '''
        Returns a polygon around the coordinates given for the object, taking
        into account angle as well.
        '''
        return get_generic_polygon(self.x, self.y, self.angle, self.width, self.length)

    def __repr__(self):
        return ('x: %s\ny: %s\nangle: %s\nvelocity: %s\ndimensions: %s\n' %
                (self.x, self.y,
                 self.angle, self.velocity, (self.width, self.length, self.height)))

