from Polygon.cPolygon import Polygon
from vision.vision import get_calibrations

class Pitch(object):
    '''
    Class that describes the pitch
    '''
    #for room 1 the zones are a bit to the left, you can see the yellow lines
    #need to change the callibration file
    def __init__(self, calibrations_file):
        config_json = get_calibrations(calibrations_file)
        croppings = config_json['croppings']

        line1 = config_json['field_croppings']['line1']
        line2 = config_json['field_croppings']['line2']
        line3 = config_json['field_croppings']['line3']

        self._width = croppings['x2'] - croppings['x1']
        self._height = croppings['y2'] - croppings['y1']
        # Getting the zones:
        self._zones = []

        # Zone 0 (left)
        zone_0_right = line1
        self._zones.append(Polygon([(0,            0), (0           , self._height), (zone_0_right, self._height), (zone_0_right, 0)]))
        print 'Zone 0', self._zones[0]

        # Zone 1
        zone_1_right = line2
        self._zones.append(Polygon([(zone_0_right, 0), (zone_0_right, self._height), (zone_1_right, self._height), (zone_1_right, 0)]))
        print 'Zone 1', self._zones[1]

        # Zone 2
        zone_2_right = line3
        self._zones.append(Polygon([(zone_1_right, 0), (zone_1_right, self._height), (zone_2_right, self._height), (zone_2_right, 0)]))
        print 'Zone 2', self._zones[2]

        # Zone 3
        zone_3_right = self._width
        self._zones.append(Polygon([(zone_2_right, 0), (zone_2_right, self._height), (zone_3_right, self._height), (zone_3_right, 0)]))
        print 'Zone 3', self._zones[3]

    def is_within_bounds(self, robot, x, y):
        '''
        Checks whether the position/point planned for the robot is reachable
        '''
        zone = self._zones[robot.zone]
        return zone.isInside(x, y)

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def zones(self):
        return self._zones

    def __repr__(self):
        return str(self._zones)
