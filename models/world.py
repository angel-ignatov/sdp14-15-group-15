from pitch import Pitch
from goal import Goal
from robot import Robot
from ball import Ball

from math import cos, sin, hypot, pi, atan2


class World(object):
    '''
    This class describes the environment
    '''
    OUR_ATTACKER = 0
    OUR_DEFENDER = 1
    THEIR_ATTACKER = 2
    THEIR_DEFENDER = 3

    def __init__(self, our_side, pitch_num):
        assert our_side in ['left', 'right']
        self._pitch = Pitch(pitch_num)
        self._our_side = our_side
        self._their_side = 'left' if our_side == 'right' else 'right'
        self._ball = Ball(0, 0, 0, 0)
        self._robots = []
        self._robots.append(Robot(self._pitch, 0, 0, 0, 0, 0))
        self._robots.append(Robot(self._pitch, 1, 0, 0, 0, 0))
        self._robots.append(Robot(self._pitch, 2, 0, 0, 0, 0))
        self._robots.append(Robot(self._pitch, 3, 0, 0, 0, 0))
        self._goals = []

        if pitch_num == 0:
            goal_top = self._pitch.height/2.0
        else:
            goal_top = (self._pitch.height/2.0) + 10

        self._goals.append(Goal(0, 0, goal_top, 0))
        self._goals.append(Goal(3, self._pitch.width, goal_top, pi))

    @property
    def our_side(self):
        return self._our_side

    @property
    def _our_attacker_index(self):
        return 2 if self._our_side == 'left' else 1

    @property
    def our_attacker(self):
        return self._robots[self._our_attacker_index]

    @property
    def _their_attacker_index(self):
        return 1 if self._our_side == 'left' else 2

    @property
    def their_attacker(self):
        return self._robots[self._their_attacker_index]

    @property
    def _our_defender_index(self):
        return 0 if self._our_side == 'left' else 3

    @property
    def our_defender(self):
        return self._robots[self._our_defender_index]

    @property
    def _their_defender_index(self):
        return 3 if self._our_side == 'left' else 0

    @property
    def their_defender(self):
        return self._robots[self._their_defender_index]

    @property
    def ball(self):
        return self._ball

    @property
    def our_goal(self):
        return self._goals[0] if self._our_side == 'left' else self._goals[1]

    @property
    def their_goal(self):
        return self._goals[1] if self._our_side == 'left' else self._goals[0]

    @property
    def pitch(self):
        return self._pitch

    @property    
    def ball_position(self): 
        zone = self.ball.current_zone(self._pitch)
        if zone == -1:
            return None
        if self._robots[zone] == self.our_attacker:
            return self.OUR_ATTACKER
        elif self._robots[zone] == self.our_defender:
            return self.OUR_DEFENDER
        elif self._robots[zone] == self.their_attacker:
            return self.THEIR_ATTACKER
        elif self._robots[zone] == self.their_defender:
            return self.THEIR_DEFENDER
    
    def print_world(self):
        '''print "Robot 0 reporting:\n"
        print self._robots[0].__repr__()
        print "Robot 1 reporting:\n"
        print self._robots[1].__repr__()
        print "Robot 2 reporting:\n"
        print self._robots[2].__repr__()
        print "Robot 3 reporting:\n"
        print self._robots[3].__repr__()
        print "Ball reporting:\n"
        print self._ball.__repr__()
        print "Goal 0 reporting:\n"
        print self._goals[0].__repr__
        print "Goal 1 reporting:\n"
        print self._goals[0].__repr__'''
        print self._ball.current_zone(self._pitch)
        print self.our_attacker.can_grab_ball(self._ball)

    def print_grabber(self):
        print self.our_defender.can_grab_ball

    def update_positions(self, pos_dict):
        ''' This method will update the positions of the pitch objects
            that it gets passed by the vision system '''

        if pos_dict['ball']['center']:
          pos_dict['ball']['center'] = (pos_dict['ball']['center'][0], self.pitch.height - pos_dict['ball']['center'][1])
        
        for robot in pos_dict['robots']:
          robot['center'] = (robot['center'][0], self.pitch.height - robot['center'][1])
            
        self.our_attacker.position = pos_dict['robots'][self._our_attacker_index]
        self.their_attacker.position = pos_dict['robots'][self._their_attacker_index]
        self.our_defender.position = pos_dict['robots'][self._our_defender_index]
        self.their_defender.position = pos_dict['robots'][self._their_defender_index]
        
        if pos_dict['ball']:
            self.ball.position = pos_dict['ball']

        # Checking if the robot locations make sense:
        # Is the side correct:
        if (self._our_side == 'left' and not(self.our_defender.x < self.their_attacker.x
            < self.our_attacker.x < self.their_defender.x)):
            print "WARNING: The sides are probably wrong!"
        if (self._our_side == 'right' and not(self.our_defender.x > self.their_attacker.x
            > self.our_attacker.x > self.their_defender.x)):
            print "WARNING: The sides are probably wrong!"
