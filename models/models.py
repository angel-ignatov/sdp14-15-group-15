# -*- coding: utf-8 -*-
from Polygon.cPolygon import Polygon
from collections import namedtuple
Point = namedtuple('Point', ['x', 'y'])

# returns top and bottom point of zone
def zone_edges(zone):
    # top and bottom are meant as what is displayed top and bottom in the GUI
    zone_points = zone[0]
    # sort zone points by y axis
    sorted_zone_points = tuple(sorted(zone_points, key=lambda point: point[1]))
    # sort top & bottom points by x axis
    bottom_points = tuple(sorted(sorted_zone_points[0:2], key=lambda point: point[0]))
    top_points = tuple(sorted(sorted_zone_points[2:4], key=lambda point: point[0]))
    # currently taking left points change these 0 to 1s to take right most points
    return [top_points[0], bottom_points[0]]


def furthest_zone_point(zone, our_position):
    candidates = zone_edges(zone)

    d0 = abs(our_position[1] - candidates[0][1])
    d1 = abs(our_position[1] - candidates[1][1])

    if d0 > d1:
        return candidates[0]
    else:
        return candidates[1]

#Moved get generic_polygon, it is no longer part of the class
#and can be called normally in case we need to build more polygons
def get_generic_polygon(x, y, angle, width, length):
        '''
        Get polygon drawn around an object with the given 
        dimensions.
        '''
        front_left = (x + length/2, y + width/2)
        front_right = (x + length/2, y - width/2)
        back_left = (x - length/2, y + width/2)
        back_right = (x - length/2, y - width/2)
        poly = Polygon((front_left, front_right, back_left, back_right))
        poly.rotate(angle, x, y)
        return poly[0]

def get_key_goal_targets(goal, goal_width):
    middle_point = Point(goal.x, goal.y)
    bottom_point = Point(goal.x, goal.y - goal_width)
    top_point = Point(goal.x, goal.y + goal_width)
    return [middle_point, top_point, bottom_point]

#set up the world state by calling
#self.world = World(our_side, pitch)
#self.world.our_defender.grabber_dimensions = {'width' : 30, 'height' : 30, 'front_offset' : 12} #10
#self.world.our_attacker.grabber_dimensions = {'width' : 30, 'height' : 30, 'front_offset' : 10}

#to check which zone the ball is in call
#world.ball.current_zone(world.pitch)
#note it will return -1 if it is on a boundary
#or if it cannot be found

#to check if robot can grab ball call
#world.our_attacker.can_grab_ball(world.ball) - change if it is defender

