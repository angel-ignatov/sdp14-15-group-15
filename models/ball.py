from pitch_object import PitchObject

BALL_WIDTH = 5
BALL_LENGTH = 5
BALL_HEIGHT = 5

class Ball(PitchObject):

    def __init__(self, x, y, angle, velocity):
        super(Ball, self).__init__(x, y, angle, velocity, BALL_WIDTH, BALL_LENGTH, BALL_HEIGHT)
        _is_caught = False

    @property
    def is_caught(self):
        return self._is_caught

    @is_caught.setter
    def is_caught(self, boolean):
        if boolean == True or boolean == False:
            _is_caught = boolean        
    # we could check for movement based on a comparison of previous coordinates 
    # and current coordinates or we can consider the velocity. As we are calculating
    # velocity inbetween two frames out of approximately 25fps, the velocity should be
    # relatively reliable. This is easy to change if needed
    @property
    def is_moving(self):
        return self.velocity > 0

    # may not be reliable if the ball is on a boundary line
    # as then isInside() could return either True or False. If none
    # of the zones return True, the ball is on one of the 
    # boundary lines and we return -1. On the next frame, 
    # if this check is made, it should return an actual
    # zone.
    def current_zone(self, pitch):
        '''
        Given the Pitch() object this function returns
        the zone the ball is located in
        '''
        zones = pitch.zones

        if zones[0].isInside(self.x, self.y):
            return 0
        elif zones[1].isInside(self.x, self.y):
            return 1
        elif zones[2].isInside(self.x, self.y):
            return 2 
        elif zones[3].isInside(self.x, self.y):
            return 3
        else:
            return -1

    def __repr__(self):
        return ('x: %s\ny: %s\nangle: %s\nvelocity: %s\ndimensions: %s\n' %
                ( self.x, self.y,
                 self.angle, self.velocity, (self.width, self.length, self.height)))


